/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package airline;

/**
 *
 * @author Henry
 */
public class Airplane {
    private String make;
    private String model;
    private String Id;
    private int numberOfSeats;

    public Airplane() {
    }

    public Airplane(String make, String model, String Id, int numberOfSeats) {
        this.make = make;
        this.model = model;
        this.Id = Id;
        this.numberOfSeats = numberOfSeats;
    }

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }

    public String getId() {
        return Id;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }
    
    
    
}
