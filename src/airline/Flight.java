/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package airline;

/**
 *
 * @author Henry
 */
public class Flight {
    private int number;
    private String date;

    public Flight() {
    }

    public Flight(int number, String date) {
        this.number = number;
        this.date = date;
    }

    public int getNumber() {
        return number;
    }

    public String getDate() {
        return date;
    }
    
    
    
}
